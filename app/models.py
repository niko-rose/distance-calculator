"""
The model for an entry into the distances database
"""

from app import db


class Distance(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    origin = db.Column(db.String(120), index=True)
    destination = db.Column(db.String(120), index=True)
    distance = db.Column(db.String(120))
    travel_time = db.Column(db.String(120))
