"""
Navigation and layout of the three pages of the application
"""

from app import app, db
from flask import request, render_template, redirect
from app.forms import DistancesForm
from app.models import Distance
from app.distance_matrix import get_distance


@app.route("/", methods=["GET", "POST"])
@app.route("/index", methods=["GET", "POST"])
def index():
    """
    Home page of the application and where past calculations are displayed
    """
    form = DistancesForm()

    if request.method == "POST" and form.validate():
        origin = form.origin.data
        destination = form.destination.data

        dist = Distance()
        results = get_distance(origin, destination)
        dist.origin = results["origin"]
        dist.destination = results["destination"]
        dist.distance = results["distance"]
        dist.travel_time = results["travel_time"]

        db.session.add(dist)
        db.session.commit()

        display = Distance.query.all()
        return render_template(
            "index.html", title="Home", form=form, display=display, current=dist
        )

    else:
        display = Distance.query.all()
        return render_template("index.html", title="Home", form=form, display=display)


@app.route("/clear/", methods=["POST"])
def clear_database():
    """
    Page that clears all past entries from the database
    """
    entries = Distance.query.all()
    for entry in entries:
        db.session.delete(entry)

    db.session.commit()
    return redirect("/index")
