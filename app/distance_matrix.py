"""
Handling and formatting for requests made to the Google Distance Matrix API
as well as formatting and return of collected data
"""

import os
import json
import time
import urllib.error
import urllib.parse
import urllib.request

from secret import API_KEY

BASE_URL = "https://maps.googleapis.com/maps/api/distancematrix/json"


def format_request(origin, destination):
    """
    Correctly formats the request URL to post to Google's API
    Returns the proper URL

    Args:
        origin: the origin (a city name)
        destination: the destination (a city name)

    Returns:
        The url that makes the proper request to the API
    """
    params = urllib.parse.urlencode(
        {
            "origins": origin,
            "destinations": destination,
            "key": API_KEY,
        }
    )
    url = f"{BASE_URL}?{params}"
    return url


def get_distance(origin, destination):
    """
    Takes origin and destination inputs and makes the request to
    Google's Distance Matrix API in order to get the distance and
    travel time between them

    Args:
        origin: the origin (a city name)
        destination: the destination (a city name)

    Returns:
        A dictionary containing the origin, destination,
    distance, and travel time
    """
    url = format_request(origin, destination)
    current_delay = 0.1
    max_delay = 5

    while True:
        try:
            response = urllib.request.urlopen(url)
        except urllib.error.URLError:
            pass
        else:
            result = json.load(response)

            if result["status"] == "OK":
                result = {
                    "origin": result["origin_addresses"][0],
                    "destination": result["destination_addresses"][0],
                    "distance": result["rows"][0]["elements"][0]["distance"]["text"],
                    "travel_time": result["rows"][0]["elements"][0]["duration"]["text"],
                }
                return result
            elif result["status"] != "UNKNOWN_ERROR":
                raise Exception(result["error_message"])

        if current_delay > max_delay:
            raise Exception("Too many retry attempts.")

        print("Waiting", current_delay, "seconds before retrying.")

        time.sleep(current_delay)
        current_delay *= 2
