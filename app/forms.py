"""
The layout of the form that takes in an origin and destination from the user
"""

from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import DataRequired


class DistancesForm(FlaskForm):
    origin = StringField(
        "Origin",
        validators=[DataRequired()],
        render_kw={"placeholder": "ex: Albuquerque, NM"},
    )
    destination = StringField(
        "Destination",
        validators=[DataRequired()],
        render_kw={"placeholder": "ex: Chicago, IL"},
    )
    submit = SubmitField("Calculate")
