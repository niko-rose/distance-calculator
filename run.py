"""
Easily runs the application on its own expected port
"""

from app import app

app.run(debug=True, host="0.0.0.0", port=8080)